from django.urls import path
from .views import *

urlpatterns = [
    path('list-vacancies/', ListVacancies.as_view()),
    path('create-vacancy/', CreateVacancy.as_view()),
    path('get-vacancy/<int:pk>/', GetVacancy.as_view()),
    path('delete-vacancy/<int:pk>/', DeleteVacancy.as_view()),
    path('update-vacancy/int:pk/', UpdateVacancy.as_view())
]