# Generated by Django 4.2.1 on 2023-05-26 17:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Candidate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_id', models.CharField(blank=True, max_length=200, null=True, verbose_name='айди из другого источника')),
                ('status', models.CharField(choices=[('new', 'новый'), ('consideration', 'На рассмотрении у заказчика'), ('testing', 'Тестовое задание'), ('interview', 'Собеседование'), ('negotiating_offer', 'Согласование оффера'), ('offer_accepted', 'Оффер принят'), ('control', 'Проверка СБ'), ('Registered', 'Оформлен'), ('bank_rejection', 'Отказ заказчика'), ('candidate_rejection', 'Отказ кандидата')], default='new', max_length=50)),
                ('first_name', models.CharField(max_length=100, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=100, verbose_name='Фамилия')),
                ('father_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Отчество')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Почта')),
                ('phone', models.CharField(blank=True, max_length=10, null=True, verbose_name='телефон')),
                ('links', models.TextField(blank=True, null=True, verbose_name='Ссылки')),
                ('photo', models.ImageField(blank=True, null=True, upload_to='')),
                ('resume', models.FileField(blank=True, null=True, upload_to='', verbose_name='файл резюме')),
            ],
            options={
                'verbose_name': 'Кандидат',
            },
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Департамент',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Проект',
            },
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
                ('description', models.TextField(verbose_name='Описание')),
            ],
            options={
                'verbose_name': 'Источник',
            },
        ),
        migrations.CreateModel(
            name='Stream',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Направление',
            },
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Подразделение',
            },
        ),
        migrations.CreateModel(
            name='Vacancy',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Название вакансии')),
                ('deadline_date', models.DateTimeField(blank=True, null=True, verbose_name='Желаемая дата закрытия вакансии')),
                ('status', models.CharField(choices=[('progress', 'В работе'), ('pause', 'На паузе'), ('done', 'Закрыта'), ('archive', 'В архиве')], max_length=10, verbose_name='статус вакансии')),
                ('status_date', models.DateTimeField(blank=True, null=True, verbose_name='дата смены статуса')),
                ('external_id', models.CharField(blank=True, max_length=100, null=True, verbose_name='номер или идентификатор вакансии из ваших систем')),
                ('salary_from', models.PositiveIntegerField(blank=True, null=True, verbose_name='Зарплата с')),
                ('salary_to', models.PositiveIntegerField(blank=True, null=True, verbose_name='Зарплата по')),
                ('city', models.CharField(blank=True, max_length=100, null=True, verbose_name='Город')),
                ('responsibilities', models.TextField(blank=True, null=True, verbose_name='Обязанности')),
                ('requirements', models.TextField(blank=True, null=True, verbose_name='Требования')),
                ('conditions', models.TextField(blank=True, null=True, verbose_name='Условия работы')),
                ('comments', models.TextField(blank=True, null=True, verbose_name='Комментарий или заметки')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='дата создания вакапсии')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата обновления вакансии')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='дата удаления вакансии')),
                ('department', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.department', verbose_name='Департамент')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.project', verbose_name='Проект')),
                ('stream', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.stream', verbose_name='Направление')),
                ('unit', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.unit', verbose_name='Подразделение')),
            ],
            options={
                'verbose_name': 'Вакансия',
            },
        ),
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(auto_now=True)),
                ('description', models.TextField(blank=True, null=True, verbose_name='Отчет')),
                ('candidate', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.candidate', verbose_name='Кандидат')),
                ('recruiter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='рекрутер')),
            ],
            options={
                'verbose_name': 'Кандидат',
            },
        ),
        migrations.CreateModel(
            name='Recruiter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('job_position', models.CharField(blank=True, max_length=100, null=True, verbose_name='Должность')),
                ('city', models.CharField(blank=True, max_length=100, null=True, verbose_name='Город')),
                ('photo', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Фото')),
                ('timezone', models.CharField(blank=True, max_length=3, null=True, verbose_name='Часовой пояс')),
                ('department', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.department')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Рекрутер',
            },
        ),
        migrations.CreateModel(
            name='EventReminder',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('description', models.TextField(blank=True, null=True, verbose_name='Напоминание')),
                ('candidate', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.candidate', verbose_name='Кандидат')),
                ('recruiter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='рекрутер')),
            ],
            options={
                'verbose_name': 'Напоминание',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('description', models.TextField(blank=True, null=True, verbose_name='Описание')),
                ('candidate', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.candidate', verbose_name='Кандидат')),
                ('recruiter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='рекрутер')),
            ],
            options={
                'verbose_name': 'Кандидат',
            },
        ),
        migrations.AddField(
            model_name='candidate',
            name='source',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.source', verbose_name='Источник'),
        ),
    ]
