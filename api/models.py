from django.db import models
from django.contrib.auth.models import User


class VacancyStatus(models.TextChoices):
    progress = 'progress', 'В работе'
    pause = 'pause', 'На паузе'
    done = 'done', 'Закрыта'
    archive = 'archive', 'В архиве'


class Department(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название')

    class Meta:
        verbose_name='Департамент'

class Stream(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название')

    class Meta:
        verbose_name='Направление'

class Project(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название')

    class Meta:
        verbose_name="Проект"

class Unit(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название')

    class Meta:
        verbose_name="Подразделение"


class Vacancy(models.Model):
    title = models.CharField(max_length=200, verbose_name='Название вакансии')
    deadline_date = models.DateTimeField(null=True, blank=True, verbose_name='Желаемая дата закрытия вакансии')
    status = models.CharField(max_length=10, choices=VacancyStatus.choices,verbose_name='статус вакансии')
    status_date = models.DateTimeField(null=True, blank=True, verbose_name='дата смены статуса')
    external_id = models.CharField(null=True, blank=True, max_length=100,verbose_name='номер или идентификатор вакансии из ваших систем')
    department = models.ForeignKey(Department, null=True, blank=True, on_delete=models.SET_NULL,verbose_name='Департамент')
    stream = models.ForeignKey(Stream, null=True, blank=True, on_delete=models.SET_NULL,verbose_name='Направление')
    project = models.ForeignKey(Project, null=True, blank=True, on_delete=models.SET_NULL,verbose_name='Проект')
    unit = models.ForeignKey(Unit, null=True, blank=True, on_delete=models.SET_NULL,verbose_name='Подразделение')
    salary_from = models.PositiveIntegerField(null=True, blank=True, verbose_name='Зарплата с')
    salary_to = models.PositiveIntegerField(null=True, blank=True, verbose_name='Зарплата по')
    city = models.CharField(null=True, blank=True, max_length=100,verbose_name='Город')
    responsibilities = models.TextField(null=True, blank=True, verbose_name='Обязанности')
    requirements = models.TextField(null=True, blank=True, verbose_name='Требования')
    conditions = models.TextField(null=True, blank=True, verbose_name='Условия работы')
    comments = models.TextField(null=True, blank=True, verbose_name='Комментарий или заметки')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='дата создания вакапсии')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления вакансии')
    deleted_at = models.DateTimeField(null=True, blank=True, verbose_name='дата удаления вакансии')

    class Meta:
        verbose_name='Вакансия'


class Source(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')

    class Meta:
        verbose_name='Источник'

class CandidateStatus(models.TextChoices):
    new = 'new', 'новый'
    consideration = 'consideration', 'На рассмотрении у заказчика'
    testing = 'testing', 'Тестовое задание'
    interview = 'interview', 'Собеседование'
    negotiating_offer = 'negotiating_offer', 'Согласование оффера'
    offer_accepted = 'offer_accepted', 'Оффер принят'
    control = 'control', 'Проверка СБ'
    Registered = 'Registered', 'Оформлен'
    bank_rejection = 'bank_rejection', 'Отказ заказчика'
    candidate_rejection = 'candidate_rejection', 'Отказ кандидата'


class Candidate(models.Model):
    external_id = models.CharField(max_length=200, verbose_name='айди из другого источника', blank=True, null=True)
    source = models.ForeignKey(Source, on_delete=models.SET_NULL, verbose_name='Источник', blank=True, null=True)
    status = models.CharField(max_length=50, choices=CandidateStatus.choices, default=CandidateStatus.new)
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    father_name = models.CharField(max_length=100, verbose_name='Отчество', null=True, blank=True)
    email = models.EmailField(verbose_name='Почта', null=True, blank=True)
    phone = models.CharField(max_length=10, verbose_name='телефон', null=True, blank=True)
    links = models.TextField(verbose_name='Ссылки', null=True, blank=True)
    photo = models.ImageField(null=True, blank=True)
    resume = models.FileField(verbose_name='файл резюме', null=True, blank=True)

    class Meta:
        verbose_name='Кандидат'

class Report(models.Model):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE, verbose_name='Кандидат')
    recruiter = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='рекрутер')
    date = models.DateField(auto_now=True)
    description = models.TextField(verbose_name='Отчет', null=True, blank=True)

    class Meta:
        verbose_name='Кандидат'

class Event(models.Model):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE, verbose_name='Кандидат')
    recruiter = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='рекрутер')
    date = models.DateField()
    description = models.TextField(verbose_name='Описание', null=True, blank=True)

    class Meta:
        verbose_name='Кандидат'

class EventReminder(models.Model):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE, verbose_name='Кандидат')
    recruiter = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='рекрутер')
    date = models.DateField()
    description = models.TextField(verbose_name='Напоминание', null=True, blank=True)

    class Meta:
        verbose_name='Напоминание'

class Recruiter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    job_position = models.CharField(max_length=100, verbose_name='Должность', null=True, blank=True)
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True, blank=True)
    city = models.CharField(max_length=100, verbose_name='Город', null=True, blank=True)
    photo = models.ImageField(verbose_name="Фото", null=True, blank=True)
    timezone = models.CharField(max_length=3, verbose_name='Часовой пояс', null=True, blank=True)

    class Meta:
        verbose_name='Рекрутер'