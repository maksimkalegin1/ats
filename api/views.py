from datetime import datetime

from rest_framework.response import Response
from rest_framework import authentication, permissions, generics, mixins
from django.contrib.auth.models import User

from .serializers import *
from .models import *


# Вакансии

class ListVacancies(generics.ListAPIView):
    """
    Список всех вакансий
    """
    queryset = Vacancy.objects.all()
    serializer_class = VacancySerializer

class CreateVacancy(generics.CreateAPIView):
    """
    Создание Вакансии
    """
    serializer_class = VacancySerializer


class GetVacancy(generics.RetrieveAPIView):
    """
    Получение данных вакансии по айди
    """
    serializer_class = VacancySerializer
    queryset = Vacancy.objects.all()
    lookup_field = 'pk'


class DeleteVacancy(generics.RetrieveAPIView):
    """
    Добавление даты удаления в вакансии
    """
    serializer_class = VacancySerializer
    queryset = Vacancy.objects.all()
    lookup_field = 'pk'

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.deleted_at = datetime.now()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class UpdateVacancy(generics.RetrieveUpdateAPIView):
    """
    Изменение данных вакаснии
    """
    serializer_class = VacancySerializer
    queryset = Vacancy.objects.all()
    lookup_field = 'pk'


# Кандидаты

class CreateCandidate(generics.CreateAPIView):
    serializer_class = CandidateSerializer


class ListCandidates(generics.ListAPIView):
    serializer_class = CandidateSerializer

    def get_queryset(self):
        pass