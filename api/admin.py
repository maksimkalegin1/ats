from django.contrib import admin

from .models import *

admin.site.register(Department)
admin.site.register(Stream)
admin.site.register(Project)
admin.site.register(Unit)
admin.site.register(Vacancy)
admin.site.register(Source)
admin.site.register(Candidate)
admin.site.register(Report)
admin.site.register(Event)
admin.site.register(EventReminder)
admin.site.register(Recruiter)
# Register your models here.
