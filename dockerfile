FROM python:3.10

ENV PYTHONUNBUFFERED 1
RUN mkdir /ats
WORKDIR /ats
COPY . /ats/
RUN pip install -r requirements.txt